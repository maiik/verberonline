tns_ind_presente	Presente simple
tns_ind_pret		Pretérito indefinido (simple)
tns_ind_fut			Futuro simple
tns_cond			Condicional
tns_ind_imper		Imperfecto
tns_ind_pres_prog	Presente progresivo
tns_ind_pretper		Pretérito perfecto	
tns_ind_plusc		Pluscuamperfecto		
tns_ind_futper		Futuro perfecto
tns_condper			Condicional perfecto
tns_ind_pretant		Pretérito anterior

tns_subj_pres		Subjuntivo Presente
tns_subj_imp		Subjuntivo Imperfecto
tns_subj_fut		Subjuntivo Futuro
tns_subj_perf		Subjuntivo Pretérito Perfecto
tns_subj_plusc		Subjuntivo Pluscuamperfecto
tns_subj_futper		Subjuntivo Futuro Perfecto

tns_imp_pos			Imperativo Positivo
tns_imp_neg			Imperativo Negativo