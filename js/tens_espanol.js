var tensesArray = [ 
  {
    "tenseType": "Indicativo",
    "tenseVars": [
    {
      "tenseId": "t_i_presente",
      "tns_nivel": "A-1",
      "tns_short": "Pres. simple",
      "tns_long": "Presente simple"
    },
    {
      "tenseId": "t_i_pret",
      "tns_nivel": "A-1",
      "tns_short": "Pretérito indefinido",
      "tns_long": "Pretérito indefinido (simple)"
    },
    {
      "tenseId": "t_i_fut",
      "tns_nivel": "A-2",
      "tns_short": "Futuro",
      "tns_long": "Futuro simple"
    },
    {
      "tenseId": "t_i_imper",
      "tns_nivel": "A-2",
      "tns_short": "Imperfecto",
      "tns_long": "Imperfecto"
    },
    {
      "tenseId": "t_i_pres_prog",
      "tns_nivel": "A-1",
      "tns_short": "Pres. progresivo",
      "tns_long": "Presente progresivo"
    },
    {
      "tenseId": "t_i_pretper",
      "tns_nivel": "A-2",
      "tns_short": "Pretérito perfecto",
      "tns_long": "Pretérito perfecto"
    },
    {
      "tenseId": "t_i_plusc",
      "tns_nivel": "B-1",
      "tns_short": "Pluscuamperfecto",
      "tns_long": "Pluscuamperfecto"
    },
    {
      "tenseId": "t_i_futper",
      "tns_nivel": "B-1",
      "tns_short": "Futuro perfecto",
      "tns_long": "Futuro perfecto"
    },
    {
      "tenseId": "t_i_pretper",
      "tns_nivel": "B-2",
      "tns_short": "Pretérito anterior",
      "tns_long": "Pretérito anterior"
    }
    ]
  },
  

  {
    "tenseType": "Imperativo",
    "tenseVars": [
    {
      "tenseId": "t_m_pos",
      "tns_nivel": "A-2",
      "tns_short": "Imp. Positivo",
      "tns_long": "Imperativo Positivo"
    },
    {
      "tenseId": "t_m_neg",
      "tns_nivel": "A-2",
      "tns_short": "Imp. Negativo",
      "tns_long": "Imperativo Negativo"
    }
    ]  
  },
  
  {
    "tenseType": "Subjuntivo",
    "tenseVars": [
    {
      "tenseId": "t_s_pres",
      "tns_nivel": "A-2",
      "tns_short": "Subjuntivo Presente",
      "tns_long": "Subjuntivo Presente"
    },
    {
      "tenseId": "t_s_imp",
      "tns_nivel": "A-2",
      "tns_short": "Subjuntivo Imperfecto",
      "tns_long": "Subjuntivo Imperfecto"
    },
    {
      "tenseId": "t_s_fut",
      "tns_nivel": "B-2",
      "tns_short": "Subjuntivo Futuro",
      "tns_long": "Subjuntivo Futuro"
    },
    {
      "tenseId": "t_s_perf",
      "tns_nivel": "B-2",
      "tns_short": "Subjuntivo P. Perfecto",
      "tns_long": "Subjuntivo Pretérito Perfecto"
    },
    {
      "tenseId": "t_s_plusc",
      "tns_nivel": "C-1",
      "tns_short": "Subjuntivo Pluscuamp.",
      "tns_long": "Subjuntivo Pluscuamperfecto"
    },
    {
      "tenseId": "t_s_futper",
      "tns_nivel": "C-1",
      "tns_short": "Subjuntivo Fut. Perf.",
      "tns_long": "Subjuntivo Futuro Perfecto"
    }
    ]
  },
  
  {
    "tenseType": "Condicional",
    "tenseVars": [
    {
      "tenseId": "t_c",
      "tns_nivel": "A-2",
      "tns_short": "Condicional",
      "tns_long": "Condicional"
    },
    {
      "tenseId": "t_c_per",
      "tns_nivel": "B-2",
      "tns_short": "Condicional perfecto",
      "tns_long": "Condicional perfecto"
    }
    ]
  },
];



var templateTenseType = document.querySelector('#tense-type-template');
var templateTenseTypeContent = (templateTenseType.content || templateTenseType).children[0];

var templateTense = document.querySelector('#tense-template');
var templateTenseContent = (templateTense.content || templateTense).children[0];

var container = document.querySelector('.c_timescroll');


//Группа времен
function getTenseTypeFromTemplate(data) {
  var element;
  tenseType = templateTenseTypeContent.cloneNode(true);
  tenseType.querySelector('.c_tns_h2').textContent = data.tenseType;
  container.appendChild(tenseType);  
  
  data.tenseVars.forEach(function(itemOne) {
    getTenseFromTemplate(itemOne);
  });
  
}

//Одно время
function getTenseFromTemplate(dataOne) {
    var elementOne;
    elementOne = templateTenseContent.cloneNode(true);
    elementOne.querySelector('.c_th').id = dataOne.tenseId;
    elementOne.querySelector('.tns_nivel').textContent = dataOne.tns_nivel;
    elementOne.querySelector('.tns_nivel').classList.add('tns_nivel_' + dataOne.tns_nivel);    
    elementOne.querySelector('.tns_short').textContent = dataOne.tns_short;
    elementOne.querySelector('.tns_long').textContent = dataOne.tns_long;
    container.appendChild(elementOne);
    elementOne.onclick = function() {
      timeClick(this);
    };
}

function initTenses() {
  tensesArray.forEach(function(item) {
    getTenseTypeFromTemplate(item);
  });
};
initTenses();



// ! Timetable
// ! — Hide tenses on load
function hide_tenses_on_load() {
  ix.run(ix_timeoff, $('.c_th_active'));            
  $('.c_th_active').removeClass('c_th_active');
  var tensesarray = [];
  _.each( tensesshown, function( val, key ) {
    tensesarray.push(key);
  });
  for (var i = 0; i < tensesarray.length; i++) {
    var tensename = tensesarray[i];
    var tenseid = '#' + tensename;
    if (tensesshown[tensename] == true) {
      $('.c_timetable').find(tenseid).removeClass('c_time_th_hidden');
      $('.c_timetable').find(tenseid).addClass('c_th_active');
      ix.run(ix_timeon, $('.c_th_active'));                  
//      $('.c_timetable').find(tenseid).css('{opacity:1;}');
    } else {
      $('.c_timetable').find(tenseid).addClass('c_time_th_hidden');
    }
  }
}
